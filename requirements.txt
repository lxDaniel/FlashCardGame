asgiref==3.4.1
beautifulsoup4==4.10.0
cached-property==1.5.2
certifi==2021.5.30
chardet==4.0.0
coreapi==2.3.3
coreschema==0.0.4
dj-database-url==0.5.0
Django==3.2.5
django-ckeditor==6.1.0
django-cors-headers==3.7.0
django-crispy-forms==1.12.0
django-filter==2.4.0
django-heroku==0.3.1
django-js-asset==1.2.2
django-mptt==0.12.0
django-rest-swagger==2.2.0
django-static-fontawesome==5.14.0.0
django-url-filter==0.3.15
django-widget-tweaks==1.4.8
djangorestframework==3.12.4
djangorestframework-jwt==1.11.0
enum-compat==0.0.3
gunicorn==20.1.0
idna==2.10
itypes==1.2.0
Jinja2==3.0.1
MarkupSafe==2.0.1
openapi-codec==1.3.2
Pillow==9.0.0
psycopg2==2.9.3
psycopg2-binary==2.9.1
PyJWT==1.7.1
python-decouple==3.5
pytz==2021.1
requests==2.25.1
simplejson==3.17.2
six==1.16.0
soupsieve==2.3.1
sqlparse==0.4.1
typing-extensions==3.10.0.0
uritemplate==3.0.1
urllib3==1.26.6
whitenoise==5.3.0
