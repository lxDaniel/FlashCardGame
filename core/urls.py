from django.conf.urls import url
from django.urls import path
from . import views

app_name = 'core'
urlpatterns = [
    path('', views.index, name='index'),
    url(r'^account_activation_sent/$', views.account_activation_sent, name='account_activation_sent'),
    url(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z_\\-]{1,60})/$',
        views.activate, name='activate'),
    path('meus_baralhos/', views.MeusBaralhosListView.as_view(), name='meus_baralhos'),
    path('baralho_adicionar_colaborador/<int:pk>/', views.BaralhoAdicionarColaborador.as_view(),
         name='baralho_adicionar_colaborador'),
    path('baralhos_publicos/', views.BaralhosListView.as_view(), name='public_baralhos'),
    path('cartoes_publicos/<int:pk>', views.PublicCartoesListView.as_view(), name='public_cartoes'),
    path('cartoes/<int:pk>/', views.CartaoListView.as_view(), name='cartoes'),
    path('editar_cartao/<int:pk>/', views.CartaoUpdateView.as_view(), name='editar_cartao'),
    path('criar_baralho/', views.BaralhoCreateView.as_view(), name='criar_baralho'),
    path('excluir_baralho/<pk>/', views.BaralhoDeleteView.as_view(), name='excluir_baralho'),
    path('criar_cartao/<int:baralho_pk>/', views.CartaoCreateView.as_view(), name='criar_cartao'),
    path('excluir_cartao/<pk>/', views.CartaoDeleteView.as_view(), name='excluir_cartao'),
    path('treinar_baralho/<int:baralho_pk>/', views.treinar_baralho, name='treinar_baralho'),
    path('atualizar_resposta/<cartao_pk>/', views.atualizar_resposta, name='resposta'),
    path('atualizar_resposta/<cartao_pk>/<acao>/', views.atualizar_resposta, name='resposta'),
    path('historico_revisoes/<cartao_pk>/<usuario_pk>', views.historico_revisoes, name='historico_revisoes'),
    path('acompanhamento_vinculados/<baralho_pk>', views.acompanhamento_vinculados, name='acompanhamento_vinculados'),
    path('rankings/', views.rankings, name='rankings'),
    path('alterar_visibilidade/<baralho_pk>/', views.alterar_visibilidade, name='alterar_visibilidade'),
    path('duplicar_baralho/<baralho_pk>/', views.duplicar_baralho, name='duplicar_baralho'),
    path('referenciar_baralho/<baralho_pk>/', views.referenciar_baralho, name='referenciar_baralho'),
]
