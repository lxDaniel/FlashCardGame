from datetime import datetime, timedelta

from ckeditor_uploader.fields import RichTextUploadingField
from django.contrib.auth.models import User
from django.db import models
from mptt.models import MPTTModel, TreeForeignKey


class Comum(models.Model):

    """
        Model abstrato que reúne fields comuns a mais de um Model.
        Será portanto utilizado como classe mãe de models que precisem dos fields aqui definidos.
    """
    titulo = models.CharField(verbose_name='Título', max_length=255, blank=True, null=True)
    criado_em = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    criador = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)

    class Meta:
        app_label = 'core'
        ordering = ('-id',)
        abstract = True


class Categoria(MPTTModel):
    """
        O Model categoria tem por objetivo definir a lista de categorias disponíveis para seleção.
        Posteriormente é utilizado em class Baralho. E nas views como forma de filtro por categoria.
    """
    parent = TreeForeignKey('self', related_name='children', on_delete=models.CASCADE, blank=True, null=True)
    titulo = models.CharField(verbose_name='Titulo', max_length=500)

    class MPTTMeta:
        order_insertion_by = ['titulo']
        unique_together = (('titulo', 'parent',),)

    def __str__(self):
        return self.titulo


class Baralho(Comum):
    """
    o model Baralho -que herda de Comum- define as propriedades de controle para o conjunto de cartões que
    será criado posteriormente.
    Observações:
        - O field booleano 'colaborativo' define se o baralho é colaborativo. Isso implica se pode ou náo possuir
        colaboradores.
        - O field 'colaboradores' permite criar uma lista de usuários-terceiros que poderão executar CRUD no baralho.
        - o field 'seguidores' define a lista de usuários seguidores. Esta lista é preenchida ao referenciar baralho.
    """

    descricao = models.TextField(verbose_name='Descrição', blank=True, null=True)
    publico = models.BooleanField(verbose_name='Baralho é público?', default=False)
    colaborativo = models.BooleanField(verbose_name='Baralho é Colaborativo?', default=False)
    categoria = models.ForeignKey(Categoria, on_delete=models.CASCADE)
    colaboradores = models.ManyToManyField(User, verbose_name='Usuários Colaboradores', related_name='colaborador_list')
    seguidores = models.ManyToManyField(User, verbose_name='Usuários Seguidores do Baralho', related_name='seguidor_list')

    class Meta:
        verbose_name = 'Baralho'
        verbose_name_plural = 'Baralhos'

    def __str__(self):
        return f'{self.titulo}'


class Cartao(Comum):
    """
        O cartão é o model no qual é propriamente preenchido o conteúdo a ser memorizado.
    """
    baralho = models.ForeignKey(Baralho, on_delete=models.CASCADE)
    frente = RichTextUploadingField(verbose_name='Frente do Cartão', blank=True, null=True)
    verso = RichTextUploadingField(verbose_name='Verso do Cartão', blank=True, null=True)
    imagem_capa = models.ImageField(null=True, blank=True, max_length=300)

    class Meta:
        verbose_name = 'Cartão'
        verbose_name_plural = 'Cartões'

    def __str__(self):
        return f'{self.pk} - {self.titulo}'


class UsuarioCartao(models.Model):
    """
    O Model UsuarioCartao surge como uma tabela associativa de Usuário e Cartao. Tem por objetivo relacionar
    os diversos usuários aos diversos cartões.
    O campo historico_interacoes terá por objetivo armazenar um JSON contendo a data da revisão, a dificuldade
    atribuída, e se o usuário ainda lembrava da resposta do cartão na ocasião.
            Obs:
                O campo historico_interacoes é um JSON incremental, ou seja, inicialmente vazio,
                recebe uma lista do resultado da interação atual. Após o primeiro preenchimento,
                recebe o valor já armazenado acrescido da lista de valores da interação atual.
    """

    FACIL = 1
    FACIL_MEDIO = 2
    MEDIO = 3
    MEDIO_DIFICIL = 4
    DIFICIL = 5

    DIFICULDADE_CHOICES = [
        [FACIL, 'Fácil'],
        [FACIL_MEDIO, 'Fácil-Médio'],
        [MEDIO, 'Médio'],
        [MEDIO_DIFICIL, 'Médio-Difícil'],
        [DIFICIL, 'Difícil'],
    ]

    usuario = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)
    cartao = models.ForeignKey(Cartao, on_delete=models.CASCADE)
    nivel_atual = models.PositiveSmallIntegerField(default=1)
    dificuldade_atual = models.CharField(
        verbose_name='Dificuldade Percebida',
        choices=DIFICULDADE_CHOICES,
        default='1',
        blank=True,
        max_length=10,
    )
    ultima_revisao = models.DateTimeField(verbose_name='Última Revisão em', blank=True, null=True)
    proxima_revisao = models.DateTimeField(verbose_name='Próxima Revisão em', blank=True, null=True)
    quantidade_revisoes = models.PositiveIntegerField(verbose_name='Quantidade de Revisões do Cartão', default=0,
                                                      blank=True)
    acertos = models.PositiveIntegerField(verbose_name='Quantidade de Confirmações de Memorização', default=0,
                                          blank=True)
    erros = models.PositiveIntegerField(verbose_name='Quantidade de Confirmações de Esquecimento', default=0,
                                        blank=True)
    historico_interacoes = models.JSONField(null=True, blank=True)
    streak = models.PositiveIntegerField(verbose_name='Maior sequência de acertos', default=0, blank=True)

    def get_proxima_revisao(self):
        return datetime.today() + timedelta(days=2 ** (self.nivel_atual - 1)) \
            if self.nivel_atual < 6 else datetime.today() + timedelta(days=30)
