from django.contrib import admin
from core.models import *


@admin.register(Categoria)
class CategoriaAdmin(admin.ModelAdmin):
    """
        Categoria é um registro mais sensível e deve ser
        criada ou editada somente por usuários específicos,
        que possuam permissão para visualizar o DjangoAdmin.
    """
    pass
