from datetime import date, timedelta

from django import template

register = template.Library()


def gitlab_date(value):
    """ Retorna a data da API do gitlab em DD/MM/YYYY. """
    git_data = value[0:10]
    data = date.fromisoformat(git_data)
    return data.strftime("%d/%m/%Y")


register.filter('gitlab_date', gitlab_date)


def somar_dias(value, dias):
    """ Retorna data somada da quantidade de dias recebida no parâmetro da função """
    return value + timedelta(days=dias)


register.filter('somar_dias', somar_dias)
