from django.contrib.auth import login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.contrib import messages
from django.contrib.sites.shortcuts import get_current_site
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.db.models import F, Count, Sum
from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404, redirect
from django.template.loader import render_to_string
from django.urls import reverse
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.views import generic
from django_filters.views import FilterView
from .forms import CartaoCreateForm, BaralhoCreateForm, BaralhoColaboradorForm, RegistroUsuarioForm
from core.utils import get_imagem_capa, vinculo_usuario_cartao, vinculo_usuario_cartao_referenciado
from core.models import UsuarioCartao, Baralho, Cartao, User
from datetime import datetime, timedelta
import requests
from home import settings
from .tokens import account_activation_token


def main(request):
    response = requests.get(f'https://gitlab.com/api/v4/projects/25288440/repository/commits?').json()[:3]
    url_projeto = settings.URL_PROJETO
    nome_projeto = settings.NOME_PROJETO
    cartoes = Cartao.objects.all().count()
    baralhos = Baralho.objects.all().count()
    usuarios = User.objects.filter(is_superuser=False).count()
    return render(request, 'core/main.html', context=locals())


def about(request):
    return render(request, 'core/about.html', context=locals())


@login_required()
def index(request):
    cartoes = UsuarioCartao.objects.filter(
        usuario=request.user,
    )
    revisoes = cartoes.aggregate(Sum('quantidade_revisoes'))
    cartoes_disponiveis_revisao = cartoes.filter(
        proxima_revisao__lte=datetime.today()
    ).order_by('cartao__baralho')
    cartoes_revisao = cartoes_disponiveis_revisao.count()
    cartoes_semana = cartoes.filter(
        proxima_revisao__gte=datetime.today().date(),
        proxima_revisao__lte=datetime.today().date()+timedelta(days=7)
    ).order_by('cartao__baralho')
    baralho_vinculo = cartoes.filter().distinct('cartao__baralho')
    baralho_count = baralho_vinculo.count() if baralho_vinculo else \
        Baralho.objects.filter(seguidores__in=[request.user]).count()

    return render(request, 'core/index.html', context=locals())


def signup(request):
    if request.method == 'POST':
        form = RegistroUsuarioForm(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            user.is_active = False
            user.save()
            current_site = get_current_site(request)
            subject = 'Ative sua conta MemoryTrigger'
            message = render_to_string('registration/account_activation_email.html', {
                'user': user,
                'domain': current_site.domain,
                'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                'token': account_activation_token.make_token(user),
            })
            user.email_user(subject, message)
            return redirect('/core/account_activation_sent')
    else:
        form = RegistroUsuarioForm()
    return render(request, 'registration/signup.html', {'form': form})


def activate(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except (TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None

    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()
        login(request, user)
        return redirect('/core/')
    else:
        return render(request, 'registration/account_activation_invalid.html')


def account_activation_sent(request):
    return render(request, 'registration/account_activation_sent.html')


class BaralhoCreateView(LoginRequiredMixin, generic.CreateView):
    model = Baralho
    form_class = BaralhoCreateForm
    template_name = 'core/baralho_form.html'

    def form_valid(self, form):
        form.instance.criador = self.request.user
        self.object = form.save()
        form.instance.seguidores.add(self.request.user)
        form.instance.colaboradores.add(self.request.user)
        self.object = form.save()
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('core:meus_baralhos')


class MeusBaralhosListView(LoginRequiredMixin, generic.ListView):
    template_name = 'core/meus_baralhos.html'
    paginate_by = 4

    def get_context_data(self, **kwargs):
        context = super(MeusBaralhosListView, self).get_context_data(**kwargs)
        context['cartoes'] = UsuarioCartao.objects.filter(usuario=self.request.user)
        return context

    def get_queryset(self):
        return Baralho.objects.filter(seguidores__in=[self.request.user]).order_by('pk')


class BaralhosListView(LoginRequiredMixin, FilterView):
    """
     View para listar todos os Baralhos com status 'Público'
    """
    model = Baralho
    template_name = 'core/public_baralhos.html'

    def get_context_data(self, **kwargs):
        context = super(BaralhosListView, self).get_context_data(**kwargs)
        context['hoje'] = datetime.now()
        return context

    def get_queryset(self):
        return Baralho.objects.filter(publico=True).order_by('pk')


class BaralhoDeleteView(LoginRequiredMixin, UserPassesTestMixin, generic.DeleteView):
    model = Baralho
    success_url = '/core/meus_baralhos/'

    def test_func(self):
        return self.model.objects.filter(criador=self.request.user)


class BaralhoAdicionarColaborador(LoginRequiredMixin, UserPassesTestMixin, generic.UpdateView):
    """
        View para adicionar um colaborador ao Baralho.
        Um colaborador é um usuário-terceiro com permissão de CRUD no Baralho e cartões vinculados.
    """
    model = Baralho
    form_class = BaralhoColaboradorForm

    success_url = '/core/meus_baralhos/'

    def test_func(self):
        return self.model.objects.filter(criador=self.request.user)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['seguidores'] = self.model.objects.filter(pk=self.kwargs['pk']).values_list('seguidores', flat=True)
        return kwargs


class CartaoCreateView(LoginRequiredMixin, UserPassesTestMixin, generic.CreateView):
    model = Cartao
    form_class = CartaoCreateForm
    template_name = 'core/cartao_form.html'

    def form_valid(self, form):
        form.instance.usuario = self.request.user
        form.instance.baralho = Baralho.objects.get(pk=self.kwargs['baralho_pk'])
        form.instance.imagem_capa = get_imagem_capa(form)
        self.object = form.save()
        vinculo_usuario_cartao(self.request, self.object.pk)
        vinculo_usuario_cartao_referenciado(self.request, self.object.pk)
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('core:cartoes', args=(Baralho.objects.get(pk=self.kwargs['baralho_pk']).pk,))

    def test_func(self):
        return Baralho.objects.filter(criador=self.request.user) or \
               Baralho.objects.filter(colaboradores__in=[self.request.user])


class CartaoListView(LoginRequiredMixin, generic.ListView):
    template_name = 'core/cartoes.html'
    paginate_by = 6

    def get_context_data(self, **kwargs):
        context = super(CartaoListView, self).get_context_data(**kwargs)
        user = self.request.user
        baralho = Baralho.objects.get(pk=self.kwargs['pk'])
        context['baralho_pk'] = baralho.pk
        context['baralho'] = baralho
        context['pode_manipular'] = user in [baralho.criador] or user in baralho.colaboradores.all()
        return context

    def get_queryset(self):
        return Cartao.objects.filter(baralho=self.kwargs['pk'])


class CartaoUpdateView(LoginRequiredMixin, UserPassesTestMixin, generic.UpdateView):
    model = Cartao
    form_class = CartaoCreateForm

    success_url = '/core/meus_baralhos/'

    def test_func(self):
        return self.model.objects.filter(baralho__criador=self.request.user) or \
               self.model.objects.filter(baralho__colaboradores__in=[self.request.user])


class PublicCartoesListView(LoginRequiredMixin, generic.ListView):
    """
        View para listar os cartões de um Baralho com status 'Público'.
    """
    template_name = 'core/public_cartoes.html'
    paginate_by = 6

    def get_context_data(self, **kwargs):
        context = super(PublicCartoesListView, self).get_context_data(**kwargs)
        context['baralho_pk'] = Baralho.objects.get(pk=self.kwargs['pk']).pk
        return context

    def get_queryset(self):
        return Cartao.objects.filter(baralho=self.kwargs['pk'])


class CartaoDeleteView(LoginRequiredMixin, UserPassesTestMixin, generic.DeleteView):
    model = Cartao
    success_url = '/core/meus_baralhos/'

    def test_func(self):
        return self.model.objects.filter(baralho__criador=self.request.user)


@login_required()
def treinar_baralho(request, baralho_pk):
    """
        View que exibe os Cartões disponíveis para revisão. Ou seja com data de revisão igual ou anterior a data atual.
        proxima_revisao__lte=datetime.today().
        Demais detalhes no template:
        template/core/treinar_baralho.html
    """
    cartoes = UsuarioCartao.objects.filter(
        usuario=request.user,
        cartao__baralho=baralho_pk,
        proxima_revisao__lte=datetime.today()
    )

    context = {
        'cartoes': cartoes,
    }

    return render(request, 'core/treinar_baralho.html', context)


@login_required()
def atualizar_resposta(request, cartao_pk, acao=None):
    """
        Cria ou atualiza no banco de dados o resultado de uma interação da ação de revisão do cartão de
        determinado usuário. É indiferente se é um cartão original, duplicado ou referenciado,
        uma vez que o importante está na classe UsuarioCartao, que armazena essa relação.
    """
    resposta = 'acertos' if acao == '1' else 'erros'
    cartao = get_object_or_404(UsuarioCartao, cartao=cartao_pk, usuario=request.user.pk)

    movimentacao = {
        'acao': 'Treino de Revisão',
        'data': f'{datetime.now().strftime("%d/%m/%Y %H:%M:%S")}',
        'usuario': f'{request.user}',
        'resultado': f'{resposta}',
        'acertos_data': f'{cartao.acertos}',
        'erros_data': f'{cartao.erros}',
        'revisoes_data': f'{cartao.quantidade_revisoes}',
        'nivel': f'{cartao.nivel_atual}',
        'streak': f'{cartao.streak}',
    }

    if cartao.historico_interacoes is None:
        cartao.historico_interacoes = []
        movimentacao.update({'acao': 'Treino Inicial'})

    cartao.historico_interacoes.insert(0, movimentacao)

    UsuarioCartao.objects.update_or_create(
        usuario=request.user.pk,
        cartao=cartao_pk,
        defaults={
            f'{resposta}': F(f'{resposta}') + 1,
            'quantidade_revisoes': F('quantidade_revisoes') + 1,
            'ultima_revisao': datetime.now(),
            'proxima_revisao': cartao.get_proxima_revisao(),
            'nivel_atual': F('nivel_atual') + 1 if resposta == 'acertos' else F('nivel_atual') - 1,
            'historico_interacoes': cartao.historico_interacoes,
            'streak': F('streak') + 1 if resposta == 'acertos' else 0,
        }
    )
    return HttpResponseRedirect(reverse('core:treinar_baralho', args=(cartao.cartao.baralho.pk,)))


@login_required()
def historico_revisoes(request, cartao_pk, usuario_pk):
    """
        View que exibe o histórico de interações do usuário com determinado cartão.
        View relevante para o preenchimento desta view: atualizar_resposta()
    """
    cartao = get_object_or_404(UsuarioCartao, cartao=cartao_pk, usuario=usuario_pk)

    context = {
        'cartao': cartao,
    }

    return render(request, 'core/historico_revisoes.html', context)


@login_required()
def rankings(request):
    usuarios = User.objects.filter(is_superuser=False)
    ranking = []
    acertos = erros = revisoes = qtd_cartoes = 0
    for usuario in usuarios:
        lista = UsuarioCartao.objects.filter(usuario=usuario)
        for cartoes in lista:
            acertos += cartoes.acertos
            erros += cartoes.erros
            revisoes += cartoes.quantidade_revisoes
            qtd_cartoes += 1
        ranking.append(
            {'usuario': usuario, 'acertos': acertos, 'erros': erros, 'revisoes': revisoes, 'cartoes': qtd_cartoes}
        )
        acertos = erros = revisoes = qtd_cartoes = 0

    context = {
        'usuario': usuarios,
        'ranking': ranking,
        'acertos': acertos
    }

    return render(request, 'core/rankings.html', context)


@login_required()
def alterar_visibilidade(request, baralho_pk):
    """
        Altera o Baralho de Público para Privado e vice-versa.
        Somente o Criador do Baralho pode realizar esta ação.
    """
    baralho = get_object_or_404(Baralho, pk=baralho_pk)
    if baralho.criador == request.user:
        baralho.publico = not baralho.publico
        baralho.save()

    return HttpResponseRedirect(reverse('core:meus_baralhos'))


@login_required()
def acompanhamento_vinculados(request, baralho_pk):
    """
        Permite ao criador do Baralho a visualização do
        progresso dos usuários que efetuaram a ação de
        referenciar_baralho() em algum de seus baralhos.
        Para mais detalhes, verificar o template:
        templates/core/acompanhamento_vinculados.html
    """

    baralho = get_object_or_404(Baralho, pk=baralho_pk)
    cartoes = baralho.cartao_set.all()
    paginator = Paginator(cartoes, 5)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)

    if request.user not in baralho.colaboradores.all():
        messages.add_message(
            request,
            messages.ERROR,
            'Você não pode acessar este conteúdo.',
            fail_silently=True,
        )

    return render(request, 'core/acompanhamento_vinculados.html', locals())


@login_required()
def duplicar_baralho(request, baralho_pk):
    """
        Cria uma cópia no banco de dados do Baralho, seus Cartões, define o vínculo
        do usuário com todos os cartões do baralho e o inclui como colaborador e
        seguidor do Baralho.
    """
    baralho = get_object_or_404(Baralho, pk=baralho_pk)
    cartoes = baralho.cartao_set.all()
    usuario = request.user
    copia, created = Baralho.objects.get_or_create(
        pk=None,
        criador=usuario,
        defaults={
            "titulo": baralho.titulo,
            "descricao": baralho.descricao,
            "publico": False,
            "colaborativo": False,
            "categoria": baralho.categoria,
        }
    )
    copia.colaboradores.set([usuario])
    copia.seguidores.set([usuario])

    for cartao in cartoes:
        copia_cartao, created_cartao = Cartao.objects.get_or_create(
            pk=None,
            baralho=copia,
            criador=usuario,
            defaults={
                "titulo": cartao.titulo,
                "frente": cartao.frente,
                "verso": cartao.verso,
                "criador": usuario,
            }
        )
        vinculo_usuario_cartao(request, copia_cartao.pk)

    return HttpResponseRedirect(reverse('core:meus_baralhos'))


@login_required()
def referenciar_baralho(request, baralho_pk):
    """
        Segunda função que permite o aproveitamento de um Baralho e Cartões já criados.
        Diferente da duplicar_baralho(), esta função utiliza o mesmo Baralho e Cartões.
        A diferença é que apenas são criados novos vínculos aos cartões e o usuário que
        executa a ação passa a ser um seguidor do baralho referenciado.
        Isso permite que o criador do Baralho acompanhe o progresso de treino deste usuário.
    """
    usuario = request.user
    baralho = get_object_or_404(Baralho, pk=baralho_pk)
    if usuario not in baralho.seguidores.all():
        baralho.seguidores.add(usuario)
        cartoes = baralho.cartao_set.all()

        for cartao in cartoes:
            vinculo_usuario_cartao(request, cartao.pk)

    return HttpResponseRedirect(reverse('core:meus_baralhos'))
