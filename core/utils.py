from bs4 import BeautifulSoup
from home import settings
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404
from core.models import *

"""
    Utilizado para agrupar funções com potencial de reutilização.    
"""


def get_imagem_capa(form):
    """
        Retorna a imagem de capa do Cartão.
        Verifica inicialmente se há uma imagem na frente do cartão. Caso não, verifica no verso, caso também
        não exista imagem, é atribuído o valor de uma imagem padrão.
    """
    try:
        img = BeautifulSoup(form.cleaned_data.get('frente'), features="html.parser").findAll('img')[0]['src']
    except IndexError:
        try:
            img = BeautifulSoup(form.cleaned_data.get('verso'), features="html.parser").findAll('img')[0]['src']
        except IndexError:
            img = f"{settings.STATIC_URL}{settings.STATIC_IMG_DEFAULT}"
    return img


@login_required()
def vinculo_usuario_cartao(request, cartao_pk):
    """
        Cria ou atualiza o registro de usuário para o cartão.
        É um passo importante para que um mesmo cartão possa ser
        utilizado por mais de uma pessoa.
    """
    UsuarioCartao.objects.get_or_create(
        usuario=request.user,
        cartao_id=cartao_pk,
        proxima_revisao=datetime.now()
    )


@login_required()
def vinculo_usuario_cartao_referenciado(request, cartao_pk):
    """
        Permite que os usuários inscritos em determinado cartão permaneçam atualizados
        com o conteúdo atual do baralho referenciado anteriormente.
        Ou seja, a cada novo cartão é criado o vínculo em UsuárioCartao tanto para
        o criador/colaborador quanto para os usuários inscritos(referenciados).
    """
    cartao = get_object_or_404(Cartao, pk=cartao_pk)
    seguidores = cartao.baralho.seguidores.all().exclude(pk=request.user.pk)
    for user in seguidores:
        UsuarioCartao.objects.get_or_create(
            usuario=user,
            cartao_id=cartao_pk,
            proxima_revisao=datetime.now()
        )





