from crispy_forms.helper import FormHelper
from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from mptt.forms import TreeNodeChoiceField

from .models import Cartao, Baralho, Categoria


class RegistroUsuarioForm(UserCreationForm):
    email = forms.EmailField(max_length=254)

    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2', 'first_name', 'last_name')


class CartaoCreateForm(forms.ModelForm):
    class Meta:
        model = Cartao
        fields = ('titulo', 'frente', 'verso', )

        widgets = {
            'titulo': forms.TextInput(attrs={'class': 'form-control'}),
        }


class BaralhoCreateForm(forms.ModelForm):

    categoria = TreeNodeChoiceField(queryset=Categoria.objects.all())

    class Meta:
        model = Baralho
        fields = ('titulo', 'descricao', 'categoria', )

        widgets = {
            'titulo': forms.TextInput(attrs={'class': 'form-control'}),
            'descricao': forms.Textarea(attrs={'class': 'form-control'}),
        }

    def __init__(self, *args, **kwargs):
        super(BaralhoCreateForm, self).__init__(*args, **kwargs)
        self.fields['categoria'].widget.attrs['class'] = 'form-control'


class BaralhoColaboradorForm(forms.ModelForm):

    colaboradores = forms.ModelMultipleChoiceField(
        label='Selecione os usuários que poderão editar e criar cartões',
        queryset=User.objects.all(),
        widget=forms.CheckboxSelectMultiple
    )

    class Meta:
        model = Baralho
        fields = ('colaboradores', )

    def __init__(self, *args, **kwargs):
        seguidores = kwargs.pop('seguidores', None)
        super(BaralhoColaboradorForm, self).__init__(*args, **kwargs)
        self.fields['colaboradores'].queryset = User.objects.filter(pk__in=[seguidores])
