# Memory Trigger
### O que é?
O **MemoryTrigger** é um **Software OpenSource** desenvolvido com o framework [Django](https://www.djangoproject.com/) criado para possibilitar o uso e compartilhamento gratuito de **Flashcards Digitais**.
### Como surgiu?

O **MemoryTrigger** foi desenvolvido pelo acadêmico **Daniel Marques Alves de Lima** a partir da observação da possibilidade de informatização de flashcards manuais e a constatação da existência poucos softwares opensource sobre a temática. Além disso, foi utilizado em conjunto ao artigo *[O Uso de Flashcards Digitais Como Ferramenta de Estudo e Memorização](https://docs.google.com/document/d/1TcnKtroj2n2dUCgNYeIOFW8Ha1BR6o91iWuSnFXQXdk)* como trabalho de conclusão do curso de **Análise e Desenvolvimento de Sistemas** do **Instituto Federal de Educação, Ciência e Tencologia de Rondônia.**

### Funcionalidades

Além das operações básicas (**CRUD**) o MemoryTrigger permite o **compartilhamento** dos cartões criados, **acompanhamento de progresso** com a visualização do histórico de revisões individuais de cada cartão ou, caso seja um baralho público e compartilhado, o criador pode visualizar o progresso dos usuários que fazem uso de seu baralho (**baralho referenciado**). 

# Instalação 
### Softwares Requeridos

 - **Python3** - Versão 3.7.5 ou superior;
 - **Django** - Versão 3 ou superior (Atualização para Django 4 será lançada em breve com as devidas correções de compatibilidade necessárias);
 - **PostgreSQL**;

### Dependências Recomendadas

 - **PIP**;
 - **Virtualenv**;

### Passo a Passo para Deploy - Distribuições Linux ou MacOS
 * Clone o repositório:

    ```
    git clone https://gitlab.com/lxDaniel/FlashCardGame.git
    ```

 * Opcional, porém recomendado - Crie um ambiente virtual para instalação de dependências, em seguida ative-o:

    ```
    virtualenv <nomedoambiente> --python=<versaopython>
    source bin/activate/<nomedoambiente>
    ```

 * Acesse o diretório do repositório clonado e edite o arquivo .env. Configure as seguintes constantes:
    ```
	EMAIL_HOST_USER = <endereco_email> # endereço de email que irá disparar os emails necessários.
	EMAIL_HOST_PASSWORD = <senha> # senha do email.
	DB_PASSWORD = <senha_banco> # senha do banco de dados.
    ```
 * Instale as dependências:
    ```
    pip install -r requirements.txt
    ```
 * No terminal, rode as migrações:
     ```
     python manage.py migrate
     ```
 * Opcional - Crie um usuário admin:
     ```
     python manage.py createsuperuser
     ```
 * Inicie o servidor:
     ```
     python manage.py runserver
     ```
# Contribua

### Como contribuir?
Para contribuir com o desenvolvimento do **MemoryTrigger**, é possível criar uma Issue/PullRequest e submeter o código ou sugestão para avaliação da implementação, e caso aprovado será mesclado ao código da branch principal.